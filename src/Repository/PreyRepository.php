<?php

namespace App\Repository;

use App\Entity\Prey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Prey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prey[]    findAll()
 * @method Prey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Prey::class);
    }

    // /**
    //  * @return Prey[] Returns an array of Prey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prey
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

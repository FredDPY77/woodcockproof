<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LicenseRepository")
 */
class License
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $licenseNbr;

    /**
     * @ORM\Column(type="tinyint")
     */
    private $preyCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLicenseNbr(): ?string
    {
        return $this->licenseNbr;
    }

    public function setLicenseNbr(string $licenseNbr): self
    {
        $this->licenseNbr = $licenseNbr;

        return $this;
    }

    public function getPreyCount(): ?int
    {
        return $this->preyCount;
    }

    public function setPreyCount(int $preyCount): self
    {
        $this->preyCount = $preyCount;

        return $this;
    }
}

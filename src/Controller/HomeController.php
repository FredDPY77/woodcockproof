<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController

    {
        /**
         * * @Route("/home")
         */
        public function number()
        {
            $number = random_int(0, 100);
    
            return $this->render('home.html.twig', [
                'number' => $number,
            ]);
        }
    }